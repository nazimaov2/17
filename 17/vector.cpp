﻿
#include <iostream>
#include <cmath>
#include "vector.h"

Vector::Vector() : x(0), y(0), z(0)
{
}

Vector::Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
{
}

void Vector::Show()
{
	std::cout << "\n" << x << " " << y << " " << z;
}

double Vector::Module()
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}
int main()
{
	Vector v(10, 10, 10);
	v.Show();
}